/**
 *
 *
 *
 */
define([], function() {

	var ENVIRONMENT = 'test',
		appConfig = {};

	// Set vars based on environments
	switch(ENVIRONMENT) {
		case 'local' :
			appConfig.API = 'http://david-main.local.dev/dums_r/adapt/server/public';
			break;
		case 'test' :
			appConfig.API = 'http://api.dumadapt.com';
			break;
		case 'live' :
			appConfig.API = 'http://api.dumadapt.com';
			break;
	}

	return appConfig;
});
