/**
 * LOCATION view
 *
 *
 */



define([
		'jquery',
		'underscore',
		'backbone',
		'app',
		'text!templates/location-details-view.html',
		'fullcalendar'
	],
	function($, _, Backbone, app, textLocationDetailsView, fullCalendar) {

		// Location view
		var LocationView = Backbone.View.extend({
			// Events
			events : {
				'click .save-location' : 'addBookmarkDialog', // Bookmark a location
				'click #save-location-modal-btn' : 'addBookmark', // Save Bookmark
			},

			schedules : {},

			// Initialise
			initialize : function(options) {
				_.bindAll(this);
			},

			// Render
			render : function() {
				// Render location details
				locationTemplate = _.template( textLocationDetailsView , {
					location : this.model.attributes,
					session : app.SessionModel,
				});
				$(this.el).html(locationTemplate);
				return this;
			},

			onRendered : function() {
				// Render schedules
				this.renderSchedules();	
			},

			refresh : function() {
				this.render();
			},

			// Get and render schedule
			renderSchedules : function(options) {
				// Add dynamic event sources to the calendar
				app.fullCalendarConfig['events'] = {
					url : this.model.schedulesUrl(),
					cache : true,
					error : function() {
						console.log('Error occured.');
					},
					success : function(data, textStatus, jqXHR) {
						// Remove existing events before adding new ones to avoid multiple instances of the same event
						$('#location-schedule').fullCalendar('removeEvents')
						$('#location-schedule').fullCalendar('addEventSource', data.data);
					}
				};
				// Show calendar
				this.$('#location-schedule').fullCalendar(app.fullCalendarConfig);
			},

			// Show add bookmark dialog
			addBookmarkDialog : function() {
				$('#save-location-modal').modal();
			},

			// Add a new Bookmark
			addBookmark : function(event) {
				event.preventDefault();
				
				// Get Name to use for saving location
				var saveLocationName = $('#save-location-form-name').val();
				// Get location
				var location = this.model;
				// Variable to hold status of location trying to add to bookmarks
				var bookmarkExists = false;
				// Hide modal
				$('#save-location-modal').modal('hide');

				// Bookmark a location only once.
				if (app.SessionModel.user.bookmarks.size() > 0) {
					// Check if location exits in bookmarks
					app.SessionModel.user.bookmarks.each(function( bookmarkModel ) {
						// Get location of bookmark
						var bookmarkLocation = bookmarkModel.get("location");
						// Location exists as bookmark
						if (location == bookmarkLocation) {
							bookmarkExists = true;
						}

					});
				}

				// Add bookmark only if it DOESN'T already exist.
				if (!bookmarkExists || app.SessionModel.user.bookmarks.size() == 0) {
					// Instantiate model and add to namespace

					require(['models/bookmark'], function(BookmarkModel){
						var newBookmarkModel = new BookmarkModel({
							"user" : app.SessionModel.get("user_id"),
							"name" : saveLocationName,
							"location" : location.get("id"),
						});
						app.SessionModel.user.bookmarks.add(newBookmarkModel);
						newBookmarkModel.save(null, {
							success : function() {
								// Notify
								var newNotifyView = new app.NotifyView({
									notify : {
										'message' : "Bookmark: " + newBookmarkModel.get("name") + " saved successfully."
									}
								});
								// Render
								app.appNotifyView.showView(newNotifyView);
							},
							error : function() {
								// Notify
								var newNotifyView = new app.NotifyView({
									notify : {
										'message' : "Error: bookmark: " + newBookmarkModel.get("name") + " could not be saved."
									}
								});
								// Render
								app.appNotifyView.showView(newNotifyView);
							}
						}); // Save model
					});
				}

				// Return function
				return;

			}

		});
		
		return LocationView;
	}
);
