
define([
		'jquery',
		'underscore',
		'backbone',
		'app',
		'text!templates/ad-canvas-view.html',
	],
	function($, _, Backbone, app, textAdCanvas){

		var AdView = Backbone.View.extend({
			// Containing element
			// tagName : ''
			className : 'ad-canvas',


			// Initialise
			initialize : function() {
				this.refresh();
			},

			// Render
			render : function() {
				this.template = _.template( textAdCanvas , {});
				this.$el.html(this.template);
			},

			// Refresh
			refresh : function() {
				this.render();
			}
		});

		return AdView;
	}
);