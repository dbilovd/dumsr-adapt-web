/**
 * LOCATION view
 *
 *
 */

define([
		'jquery',
		'underscore',
		'backbone',
		'app',
		'text!templates/bookmark-details-view.html',
		'text!templates/bookmark-details-form-view.html',
		'fullcalendar',
		'regula',
	],
	function($, _, Backbone, app, textBookmarkDetails, textBookmarkDetailsForm, fullCalendar, regula) {

		var BookmarkDetailsView = Backbone.View.extend({
			// Fields
			errorMessages : [],

			// Events
			events : {
				'click .remove-bookmark' : 'removeBookmark', // Remove a saved location
				'click .edit-bookmark' : 'renderEditForm', // Render bookmark edit form
				'click .save-bookmark-edit' : 'updateBookmark', // Save/Update a saved location details
			},

			// Initialise
			initialize : function() {
				// Render
				this.render();
				// Event's bindings
				_.bindAll(this);
				this.model.bind('change', this.refresh); // If model changes, refresh UI
			},

			// Render
			render : function() {
				// Render location details
				bookmarkDetailsTemplate = _.template( textBookmarkDetails , {
					bookmark : this.model.toJSON(),
					session : app.SessionModel,
				});
				$(this.el).html( bookmarkDetailsTemplate );
				return this;
			},

			onRendered : function() {
				// Render fullcalendar calendar
				// this.$('#bookmark-schedule').fullCalendar(app.fullCalendarConfig);
				this.renderSchedules();
			},

			// Get and render schedule
			renderSchedules : function() {
				// Add dynamic event sources to the calendar
				app.fullCalendarConfig['events'] = {
					url : this.model.schedulesUrl(),
					cache : true,
					error : function() {
						console.log('Error occured.');
					},
					success : function(data, textStatus, jqXHR) {
						// Remove existing events before adding new ones to avoid multiple instances of the same event
						$('#bookmark-schedule').fullCalendar('removeEvents')
						$('#bookmark-schedule').fullCalendar('addEventSource', data.data);
					}
				};
				// Show calendar
				this.$('#bookmark-schedule').fullCalendar(app.fullCalendarConfig);

			},

			// Render Edit form
			renderEditForm : function() {
				console.log(this.errorMessages);
				var bookmarkEditForm = _.template( textBookmarkDetailsForm , {
					bookmark : this.model,
					errorMessages : this.errorMessages
				});
				this.$('#bookmark-details-name').html(bookmarkEditForm);
			},

			// Refresh view
			refresh : function() {
				// Render update
				this.render();
				// Render post-update
				this.onRendered();
			},

			// Update Bookmark
			updateBookmark : function( event ) {
				event.preventDefault();

				// Validate
				regula.bind();
				var validator = regula.validate({
					elements : [$('#bookmark-form-name')[0]]
				});
				if (validator.length > 0) { // Error
					this.errorMessages = [];

					// Get error messages
					for (var i = 0; i < validator.length; i++) {
						validatorFieldName = validator[i].failingElements[0].name
						this.errorMessages[validatorFieldName] = validator[i].message;
					}
					// Refresh form UI
					this.renderEditForm();
				} else { // Success
					// Get form details
					var newBookmarkName = $('#bookmark-form-name').val();
					// Update and save model
					this.model.set("name", newBookmarkName);
					this.model.save(); // Save on DB/Server
				}
			},

			// Remove Bookmark
			removeBookmark : function( event ) {
				event.preventDefault; // Prevent default

				// this.model clone
				var bookmarkToRemove = this.model;
				this.model.destroy(); // Destroy model. locally and server
				// app.BookmarksCollection.remove(this.model);
				// Redirect to locations page
				window.location.replace('#/');

				// Return function
				return;

			}
		});

		return BookmarkDetailsView;
	}
);

