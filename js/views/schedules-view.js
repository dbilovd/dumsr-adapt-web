


define([
		'jquery',
		'underscore',
		'backbone',
		'app',
	],
	function($, _, Backbone, app) {

		var SchedulesView = Backbone.View.extend({
			el : '#location-schedule',

			initialize : function() {
				console.log(this);
				_.bindAll(this);

				this.collection.bind('reset', this.refresh);
			},

			render : function() {
				console.log(this.collection);

				this.$el.fullCalendar('addEventSource', this.collection.models);
			},

			refresh : function() {
				this.render();
			}
		});

		return SchedulesView;
	}
);