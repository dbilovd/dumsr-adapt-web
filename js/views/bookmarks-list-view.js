/**
 * 
 *
 */

define([
		'jquery',
		'underscore',
		'backbone',
		'app',
		'text!templates/bookmark-list-view.html',
		'text!templates/bookmark-list-placeholder-view.html',
	],
	function($, _, Backbone, app, textBookmarkList, textBookmarkListPlaceholder) {

		// Locations view
		var BookmarksListView = Backbone.View.extend({
			// ul.nav
			tagName : 'ul',
			className : 'nav',

			// Initialise
			initialize : function() {
				_.bindAll(this);

				this.collection.bind('all', this.refresh);
				// app.SessionModel.bind('change', this.refresh);
				// app.SessionModel.bind('change', this.sessionRefresh);
			},

			render : function() {
				// Get bookmarks list template
				this.template = _.template( textBookmarkList , {
					// All locations
					bookmarks : this.collection.models
				});
				// Render template
				this.$el.html(this.template);

			    // Collapse locations list using metisMenu after adding items to list
			    app.activateMetisMenu(this.$el);
			},

			renderPlaceholder : function() {
				this.template = _.template( textBookmarkListPlaceholder );
				this.$('#bookmarks-list').html( this.template );
			},

			// Events
			events : {
				// Search locations
			},

			sessionRefresh : function() {
				console.log('Session refreshing...');
				console.log(this.collection);
				console.log(app.SessionModel);
			},

			refresh : function() {
				if (app.SessionModel.get("logged_in")) {
					this.render(); // Render list of logged in user's saved locations
				} else {
					this.renderPlaceholder(); // Restore disabled 'Saved Locations placeholder'
				}
			}

		});

		return BookmarksListView;
	}
);
