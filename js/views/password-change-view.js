


define([
		'jquery',
		'underscore',
		'backbone',
		'app',
		'regula',
		'text!templates/change-password-view.html',
		'views/notify-view',
	],
	function($, _, Backbone, app, regula, textChangePassword) {

		var PasswordChangeView = Backbone.View.extend({
			className : 'welcome',

			events : {
				'click #password-change-form-submit' : 'setNewPassword'
			},

			// Default Form Values
			formValues : {
				email : "",
			},
			// Error message
			errorMessages : [],

			initialize : function() {
			},

			render : function() {
				this.template = _.template( textChangePassword , {
					formValues : this.formValues,
					errorMessages : this.errorMessages
				});
				$(this.el).html(this.template);
				return this;
			},

			// Set error message
			setErrorMessages : function(messages) {
				var self = this;
				_.each(messages, function(message, field) {
					if (field == 'new-password-repeat') {
						field = 'newPasswordRepeat';
					}
					self.errorMessages[field] = message;
				});
				console.log(self.errorMessages);
			},

			// Login function
			setNewPassword : function (event) {
				event.preventDefault(); // Prevent Default action
				var self = this; // Access to this

				// Validate
				regula.bind();
				var validator = regula.validate({
					elements : [
						$('#password-change-form-current')[0],
						$('#password-change-form-new-password')[0],
						$('#password-change-form-new-password-repeat')[0]
					]
				});
				if (validator.length > 0) { // Error
					this.errorMessages = [];

					// Get error messages
					for (var i = 0; i < validator.length; i++) {
						validatorFieldName = validator[i].failingElements[0].name
						switch (validatorFieldName) {
							case "current-password" :
								validatorFieldName = "currentPassword";
								break;
							case "new-password" :
								validatorFieldName = "newPassword";
								break;
							case "new-password-repeat" :
								validatorFieldName = "newPasswordRepeat";
							break;
						}
						this.errorMessages[validatorFieldName] = validator[i].message;
					}
					// Refresh form UI
					this.render();
				} else { // Success

					// Get email and password
					var currentPassword = $('#password-change-form-current').val();
					var newPassword = $('#password-change-form-new-password').val();
					var newPasswordRepeat = $('#password-change-form-new-password-repeat').val();

					// Server request
					app.SessionModel.changePassword({
						'current' : currentPassword,
						'new-password' : newPassword,
						'new-password-repeat' : newPasswordRepeat
					}, 
					// Callbacks
					{
						success : function(result) {
							// if (result.messages[0]) {
								// Notify
								var newNotifyView = new app.NotifyView({
									notify : {
										'message' : result.messages[0] // First message
									}
								});
								// Render
								app.appNotifyView.showView(newNotifyView);
							// }
							// Redirect (home)
							window.location.replace('#/');
						},
						error : function(result) {
							if (result.formData) {
								self.formValues = result.formData;
							}
							self.setErrorMessages(result.messages);
							// self.errorMessages = result.messages;
							self.render(); // Render login view again with errors
						}
					});
				}

			}

		});

		return PasswordChangeView;
	}
);