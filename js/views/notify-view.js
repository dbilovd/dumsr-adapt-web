

define([
		'jquery',
		'underscore',
		'backbone',
		'app',
		'text!templates/notify-view.html',
	],
	function($, _, Backbone, app, textNotify) {

		var NotifyView = Backbone.View.extend({
			// Default timeout is 10 seconds (10000 milliseconds)
			TIMEOUT : 20000,

			// Empty notify object
			notify : {},

			// Initialise
			initialize : function(options) {
				this.notify = options.notify;
			},

			// Render
			render : function() {
				// Template
				this.template = _.template( textNotify , {
					notify : this.notify
				});
				$(this.el).html(this.template);
				return this;
			},

			// On Rendered
			onRendered : function() { }

		});
		
		// Notification view object
		app.NotifyView = NotifyView;
		// return app.NotifyView;
	}
);