

define([
		'jquery',
		'underscore',
		'backbone',
		'app',
		'text!templates/header-view.html',
		'models/session',
		'views/notify-view',
	],
	function($, _, Backbone, app, textHeaderViewTemplate) {

		var HeaderView = Backbone.View.extend({
			// Events
			events : {
				'click #user-session-logout' : 'sessionLogout'
			},

			initialize : function() {
				_.bindAll(this);

				// Listen for change in current session
				app.SessionModel.bind('change', this.refresh);
			},

			render : function() {
				var headerViewTemplate = _.template( textHeaderViewTemplate , {
					session : app.SessionModel
				});
				$(this.el).html( headerViewTemplate );
				return this;
			},

			refresh : function() {
				this.render();
			},

			sessionLogout : function() {
				app.SessionModel.logout(
					{}, // Options
					{ // Callbacks
						success : function() {
							// Notify
							var newNotifyView = new app.NotifyView({
								notify : {
									'message' : 'You have success fully logged out!'
								}
							});
							// Render
							app.appNotifyView.showView(newNotifyView);
						},
						error : function(result) {
							console.log("Error");
						}
					}
				);
			}

		});

		return HeaderView;
	}
);
