



define([
		'jquery',
		'underscore',
		'backbone',
		'app',
		'text!templates/login-form-view.html',
		'regula',
		'views/notify-view'
	],
	function($, _, Backbone, app, textLoginForm, regula) {

		var LoginView = Backbone.View.extend({
			className : 'welcome',

			events : {
				'click #login-form-submit' : 'login',
			},

			// Default Form Values
			formValues : {
				email : "",
				password : ""
			},
			// Error message
			errorMessages : [],

			initialize : function() {
				// this.render();
				// this.refresh();

				_.bindAll(this);
				app.SessionModel.bind("change", this.refresh);
			},

			render : function() {
				this.template = _.template( textLoginForm , {
					formValues : this.formValues,
					errorMessages : this.errorMessages
				});
				$(this.el).html(this.template);
				return this;

			},

			refresh : function() {
				if (app.SessionModel.get("logged_in") == false) {
					this.render();
				} else {
					// Redirecto to home page.
					window.location.replace('#/');
				}
			},

			// error messages setter
			setErrorMessages : function( messages ) {
				// Empty previous error
				this.errorMessages = [];
				// Push each new error into errors
				for (var i = 0; i < messages.length; i++) {
					var message = messages[i];
					// Wrong password
					switch (message) {
						case "Wrong password." :
							var newMessage = message + " <a href='#/recover-password'>Recover your password</a>."
							this.errorMessages.push(newMessage);
							break;
						default :
							this.errorMessages.push(message);
							break;
					}
				}
			},

			validate : function( event ) {
				var targetElement = event.currentTarget;
			},

			// Login function
			login : function (event) {
				event.preventDefault(); // Prevent Default action
				var self = this; // Access to this

				regula.bind();
				var validator = regula.validate({
					elements : [$('#login-form-email')[0], $('#login-form-password')[0]]
				});

				if (validator.length > 0) {
					this.errorMessages = [];

					// Get error messages
					for (var i = 0; i < validator.length; i++) {
						validatorFieldName = validator[i].failingElements[0].name
						this.errorMessages[validatorFieldName] = validator[i].message;
					}
					// Refresh form UI
					this.refresh();

				} else {
					// Get email and password
					var email = $('#login-form-email').val();
					var password = $('#login-form-password').val();
					
					// Log in
					app.SessionModel.login(
						{
							'email' : email,
							'password' : password
						}, 
						// Callbacks
						{
							success : function(result) {
								// Notify
								var newNotifyView = new app.NotifyView({
									notify : {
										'message' : result.messages[0] // First message
									}
								});
								// Render
								app.appNotifyView.showView(newNotifyView);
							},
							error : function(result) {
								console.log(result);
								if (result.formData) {
									self.formValues = result.formData;
								}
								self.setErrorMessages(result.messages);
								// self.errorMessages = result.messages;
								// self.render(); // Render login view again with errors
								self.refresh(); // Render login view again with errors
							}
						}
					);
				}

			}

		});

		return LoginView;
	}
);