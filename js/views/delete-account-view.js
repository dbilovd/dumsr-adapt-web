


define([
		'jquery',
		'underscore',
		'backbone',
		'app',
		'text!templates/delete-account-form-view.html',
	],
	function($, _, Backbone, app, textDeleteAccountForm) {

		var DeleteAccountView = Backbone.View.extend({
			className : 'welcome',

			events : {
				'click #delete-account-form-submit' : 'requestAccountDelete'
			},

			// Error message
			errorMessages : [],

			initialize : function() {
				this.render();
			},

			render : function() {
				this.template = _.template( textDeleteAccountForm , {
					errorMessages : this.errorMessages
				});
				$(this.el).html(this.template);
				return this;
			},

			// Login function
			requestAccountDelete : function (event) {
				event.preventDefault(); // Prevent Default action
				var self = this; // Access to this

				// Get email and password
				var email = app.SessionModel.user.get("email");
				var password = $('#delete-account-form-password').val();
				var formData = {
					'email' : email,
					'password' : password
				};

				console.log('here', formData);
				// Validate details
				app.SessionModel.loginValidate(formData, {
					success : function(result) {
						if (result.error == false) {
							console.log(result);
							console.log(app);
							var userToDelete = app.UsersCollection.get(result.data.user.id);
							if (result.data.user.id == app.SessionModel.user.id) {

								userToDelete = app.SessionModel.user;
							}
							console.log(userToDelete);
							// Instantiate user model from result and delete
							// var userToDelete = new UserModel(result.data.user);
							userToDelete.destroy({
								success : function(model, result) {
									// Notify
									var newNotifyView = new app.NotifyView({
										notify : {
											'message' : result.messages[0] // First message
										}
									});
									// Render
									app.appNotifyView.showView(newNotifyView);
									//console.log(result);
								},
								error : function(model, result) {
									console.log(model, result);
									console.log(result);
								}
							}); // Delete on server
							
							app.SessionModel.logout({}); // Logout
						} else {
							console.log('not deleting');
						}
					},
					error: function(result) {
						console.log("Error validating credentials", result);
						if (result.messages.length > 0 ){
							// Notify
							var newNotifyView = new app.NotifyView({
								notify : {
									'message' : result.messages[0] // First message
								}
							});
							// Render
							app.appNotifyView.showView(newNotifyView);
						}
					}
				})

				// Server request
				/*
				app.SessionModel.recoverPassword({
					'email' : email,
				}, 
				// Callbacks
				{
					success : function(result) {
						console.log(result);
					},
					error : function(result) {
						if (result.formData) {
							self.formValues = result.formData;
						}
						self.setErrorMessages(result.messages);
						// self.errorMessages = result.messages;
						self.render(); // Render login view again with errors
					}
				});
				*/

			}

		});

		return DeleteAccountView;
	}
);
