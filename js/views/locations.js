/**
 * LOCATIONS view
 *
 */

define([
		'jquery',
		'underscore',
		'backbone',
		'app',
		'text!templates/locations-list-view.html',
		'text!templates/locations-list-no-filter-view.html',
		'text!templates/locations-list-regions-filter-view.html',
		'collections/regions',
	],
	function($, _, Backbone, app, textLocationsList, textLocationsListNoFilter, textLocationsListRegionFilter, RegionsCollection) {
		
		var LocationsView = Backbone.View.extend({
			// Displaying of locations list is confined to div#sidebar
			// el : '#sidebar',
			tagName : 'ul',
			// id : '#locations-list',
			className : 'nav',

			initialize : function() {
				_.bindAll(this);
				this.collection.bind('reset', this.refresh, this);

				this.render();
			},

			render : function() {
				var self = this;
				// Regions filtering
				// List of regions from collection
				var regionNames = ['Accra', 'Tema', 'Ashanti', 'Central', 'Volta', 'Eastern', 'Western'];
				var regionsForSort = {};
				// Loop through region names
				_.each(regionNames, function(regionName) {
					// Get IDs of region's groups
					var regions = app.RegionsCollection.filterByName(regionName);
					if (regions._wrapped.length > 0) {
						// Variable to hold IDs of current region.
						var regionsIds = [];
						// Add region group ID to list of region's group IDs
						for (var i = 0; i < regions._wrapped.length; i++) {
							regionsIds.push(regions._wrapped[i].id);
						}
						// Filter locations by region
						// Variable to hold list of region's locations
						var regionLocations = [];
						// Make sure region's group's IDs has a value.
						if (regionsIds.length > 0) {
							// Get locations for each region group and add to the list of region locations
							_.each(regionsIds, function(regionId){
								// Locations for a single group
								var regionLocationsSingle = self.regionFilter(regionId);
								// Make sure group's locations is not empty
								if (regionLocationsSingle._wrapped.length > 0) {
									for (var j = 0; j < regionLocationsSingle._wrapped.length; j++) {
										// Add group's location to regions's locations
										regionLocations.push(regionLocationsSingle._wrapped[j].toJSON());
									}
								}
							});
						}

						regionsForSort[regionName] = {
							// Set first region as region's identity
							region : app.RegionsCollection.get(regionsIds[0]).toJSON(),
							// List of locations in this region,
							regionLocations : regionLocations
						};
					}
				});
			
				// Render template
				this.template = _.template( textLocationsList , {
					locations : this.collection.models, // All locations
					regions : regionsForSort, // List of regions and thier locations
				});
				this.$el.html(this.template);

			    app.activateMetisMenu(this.$el);

			},

			refresh : function() {
				this.render();
			},

			// Render a list of locations
			renderLocations : function( locations, regions ) {
				// Empty already existing list
				this.$el.html("");

				// Display all locations (NOT sorted)
				// Render each location and append it to the end of ul#locations-list
				_.each(locations, function(location) {
					// Render template for all locations in search result
					locationsListNoFilterTemplate = _.template( textLocationsListNoFilter , {
						locations : location
					});

					// Append 'all-locations' to location list.
					this.$('#locations-list').append(locationsListNoFilterTemplate);

				});

				// Display all locations sorted by regions
				_.each(regions, function(region) {
					// Render template for each region (region details and locations)
					locationsRegionListFilterTemplate = _.template( textLocationsListRegionFilter , {
						region : region
					});

					// Append region template to locations list.
					this.$('#locations-list').append(locationsRegionListFilterTemplate);
				    // Collapse locations list using metisMenu after adding items to list
				    app.activateMetisMenu(this.$('#locations-list'), {
				    	toggle : false
				    });
				});
				

			    // Collapse locations list using metisMenu after adding items to list
			    // this.$el.metisMenu();
			    app.activateMetisMenu(this.$('#locations-list'));

			},

			// After rendering
			onRendered : function(){
				console.log('on rendered');
				$('#locations-list').metisMenu();
			},

			// Locations search
			search : function() {
				var self = this;

				var searchTerms = $('#locations-list-search-box').val();
				if (searchTerms == '') {
					this.refresh();
					return;
				}

				// All locations that matches pattern
				var searchResultAllLocations = this.collection.search(searchTerms);

				// Region searching
				// List of regions from collection
				var searchResultRegions = [];
				var regionNames = ['Accra', 'Tema', 'Ashanti', 'Central', 'Volta', 'Eastern', 'Western'];
				var searchResultRegions = {};
				// Loop through region names
				_.each(regionNames, function(regionName) {
					// Get IDs of region's groups
					var regions = app.RegionsCollection.filterByName(regionName);
					if (regions._wrapped.length > 0) {
						// Variable to hold IDs of current region.
						var regionsIds = [];
						// Add region group ID to list of region's group IDs
						for (var i = 0; i < regions._wrapped.length; i++) {
							regionsIds.push(regions._wrapped[i].id);
						}
						// Filter locations by region
						// Variable to hold list of region's locations
						var regionLocations = [];
						// Make sure region's group's IDs has a value.
						if (regionsIds.length > 0) {
							// Get locations for each region group and add to the list of region locations
							_.each(regionsIds, function(regionId){
								// Locations for a single group
								// var regionLocationsSingle = self.regionFilter(regionId);
								var regionLocationsSingle = self.searchFilteredRegion(searchTerms, regionId);
								// Make sure group's locations is not empty
								if (regionLocationsSingle._wrapped.length > 0) {
									for (var j = 0; j < regionLocationsSingle._wrapped.length; j++) {
										// Add group's location to regions's locations
										regionLocations.push(regionLocationsSingle._wrapped[j].toJSON());
									}
								}
							});
						}

						// Only render locations with at least one location that matches search result
						if (regionLocations.length > 0) {
							searchResultRegions[regionName] = {
								// Set first region as region's identity
								region : app.RegionsCollection.get(regionsIds[0]).toJSON(),
								// List of locations in this region,
								regionLocations : regionLocations
							};
						}
					}
				});

				// Render template for all locations and region locations
				this.renderLocations( searchResultAllLocations, searchResultRegions );

			},

			// Filter locations by region
			regionFilter : function(region) {
				return this.collection.regionFilter(region);
			},

			// Search and sort by regions
			searchFilteredRegion : function(searchTerms, region) {
				return this.collection.searchFilteredRegion(searchTerms, region);
			},

		});

		return LocationsView;
	}
);
