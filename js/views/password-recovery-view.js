


define([
		'jquery',
		'underscore',
		'backbone',
		'app',
		'text!templates/recover-password-view.html',
		'regula'
	],
	function($, _, Backbone, app, textRecoverPassword, regula) {

		var PasswordRecoveryView = Backbone.View.extend({
			className : 'welcome',

			events : {
				'click #password-recovery-form-submit' : 'requestNewPassword'
			},

			// Default Form Values
			formValues : {
				email : "",
			},
			// Error message
			errorMessages : [],

			initialize : function() {
			},

			render : function() {
				this.template = _.template( textRecoverPassword , {
					formValues : this.formValues,
					errorMessages : this.errorMessages
				});
				$(this.el).html(this.template);
				return this;
			},

			// Login function
			requestNewPassword : function (event) {
				event.preventDefault(); // Prevent Default action
				var self = this; // Access to this

				// Validate
				regula.bind();
				var validator = regula.validate({
					elements : [$('#password-recovery-form-email')[0]]
				});
				if (validator.length > 0) { // Error
					this.errorMessages = [];

					// Get error messages
					for (var i = 0; i < validator.length; i++) {
						validatorFieldName = validator[i].failingElements[0].name
						this.errorMessages[validatorFieldName] = validator[i].message;
					}
					// Refresh form UI
					this.render();

				} else { // Success

					// Get email and password
					var email = $('#password-recovery-form-email').val();

					// Server request
					app.SessionModel.recoverPassword({
						'email' : email,
					}, 
					// Callbacks
					{
						success : function(result) {
							console.log(result);
						},
						error : function(result) {
							if (result.formData) {
								self.formValues = result.formData;
							}
							self.setErrorMessages(result.messages);
							// self.errorMessages = result.messages;
							self.render(); // Render login view again with errors
						}
					});
				}

			}

		});

		return PasswordRecoveryView;
	}
);