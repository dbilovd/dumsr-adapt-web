


define([
		'jquery',
		'underscore',
		'backbone',
		'app',
		'text!templates/signup-form-view.html',
		'regula'
	],
	function($, _, Backbone, app, textSignupForm, regula) {

		var SignupView = Backbone.View.extend({
			className : 'welcome',

			events : {
				'click #join-form-submit' : 'join'
			},

			// Form values
			formValues : {
				"email" : "",
				"username" : "",
				"first_name" : "",
				"last_name" : "",
			},
			// Form Errors
			formErrors : {},

			initialize : function() {
				// this.render();
			},

			render : function() {
				this.template = _.template( textSignupForm , {
					values : this.formValues,
					errors : this.formErrors
				});
				$(this.el).html(this.template);
				return this;
			},

			// Login function
			join : function (event) {
				event.preventDefault(); // Prevent default action of submit button
				var self = this;

				// Form details
				var email = $('#join-form-email').val(),
					username = $('#join-form-username').val(),
					first_name = $('#join-form-first-name').val(),
					last_name = $('#join-form-last-name').val(),
					password = $('#join-form-password').val(),
					passwordRepeat = $('#join-form-password-repeat').val();
				
				// Join
				app.SessionModel.signup({
					'email' : email,
					'username' : username,
					'first_name' : first_name,
					'last_name' : last_name,
					'password' : password,
					'password-repeat' : passwordRepeat
				},
				// Callbacks
				{
					success : function(result) {
						// Notify
						var newNotifyView = new app.NotifyView({
							notify : {
								'message' : result.messages[0]
							}
						});
						// Render
						app.appNotifyView.showView(newNotifyView);
						// Go to the home page
						window.location.replace('#/');
					},
					error : function(result) {
						// Hide loading icon
						// app.toggleRequestLoader();
						// Notify
						var newNotifyView = new app.NotifyView({
							notify : {
								'message' : "Creating Account failed. Please corrent form errors."
							}
						});
						// Render
						app.appNotifyView.showView(newNotifyView);
						
						// console.log("Signup failed.", result);
						self.formValues = result.formData;
						self.formErrors = result.messages;
						self.render(); // Re-render form
						// console.log(self);
					}
				});
			}
		});

		return SignupView;
	}
);