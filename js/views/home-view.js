


define([
    'jquery',
    'underscore',
    'backbone',
    'app',
    'text!templates/home-page-view.html',
    'moment',
  ],
  function($, _, Backbone, app, textHomePageView, moment) {

    	/***
    	 *
    	 * TIMER
    	 *
    	 *
    	 */
    function startTimer(duration, display, location) {
    	var start = Date.now(),
           	diff, 
           	hours,
           	minutes;
        	
       	function timer() {
           	// get the number of seconds that have elapsed since startTimer() was called
           	diff = duration - (((Date.now() - start) / 1000) | 0);

            // does the same job as parseInt truncates the float
            hours = ((diff / 3600) % 24) | 0; // Hours
            minutes = ((diff / 60) % 60) | 0; // Minutes

           	// Display
           	hours = hours < 10 ? "0" + hours : hours;
           	minutes = minutes < 10 ? "0" + minutes : minutes;
           	// HH:MM
            display.html('<p><a href="#/location/' + location.id +'" title="' + location.name +'" data-toggle="tooltip">Somewhere</a> in Ghana, in</p>' +
              '<h1 id="countdown-time" style="font-size: 120px;">' + hours + ":" + minutes + '</h1>' +
              '<p>the lights will go off for about 24 hours.</p>')
           	// display.textContent = hours + ":" + minutes; 

           	if (diff <= 0) {
               	// add one second so that the count down starts at the full duration
               	// example 05:00 not 04:59
               	// start = Date.now() + 1000;
           	}
       	};
        	
       	// we don't want to wait a full second before the timer starts
       	timer();
        // Update timer every 1 minute
       	setInterval(timer, 60000);
    }

    var HomeView = Backbone.View.extend({
    	className : 'welcome',
    	
    	initialize : function() {
        _.bindAll(this);
        app.LocationsCollection.bind('reset', this.showTimer);
    	},

    	render : function() {
    		// Render home template
    		var homeTemplate = _.template( textHomePageView );
    		$(this.el).html(homeTemplate);
    		return this;
    	},

    	onRendered : function() {
        this.showTimer();
    	},

      showTimer : function() {
        var randomInt = function(min, max) {
            var rand = Math.floor(Math.random() * (max - min + 1)) + min;
            return rand;
          },
          randomLocation = app.LocationsCollection.get(randomInt(1, app.LocationsCollection.models.length));

        if (randomLocation) {
          randomLocation.fetchNextSchedule({}, {
            success : function( result ) {
              // Use moment.js to wrap date for cross browser support, etc.
              resultDate = new moment(result.data[0].end, "YYYY-MM-DD HH:mm");
              // Next date
              var nextDate = new Date(resultDate);
              nextDate = nextDate.getTime();
              // Current Date
              var currentDate = Date.now();
              // Use difference between times as countdown for timer (in minutes)
              var countdownTime = (nextDate - currentDate) / 1000;
              // Start and display timer
              startTimer(countdownTime, $('.welcome-timer'), randomLocation.toJSON() );
            } 
          });
        }
      }

    });

    return HomeView;
  }
);