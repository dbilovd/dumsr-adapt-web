/**
 *
 */

define([
		'jquery',
		'underscore',
		'backbone',
		'app',
		'text!templates/sidebar-view.html',
		'collections/locations',
		'views/bookmarks-list-view',
		'views/locations',
		'views/ad-view',
	],
	function($, _, Backbone, app, textSidebarView, LocationsCollection, BookmarksListView, LocationsView, AdView) {
		
		var SidebarView = Backbone.View.extend({
			// Declare el
			el : '#sidebar-wrapper',

			// Events
			events : {
				// Search locations
				'keyup #locations-list-search-box' : 'search',
				
			},

			// locations : {},

			initialize : function( options ) {
				this.locations = app.LocationsCollection = new LocationsCollection();

				// Render sidebar
				this.render();

				// Bookmarks list
				this.BookmarksListView = new BookmarksListView({
					el : '#bookmarks-list',
					collection : app.SessionModel.user.bookmarks
				});

				// new Locations list view
				this.LocationsView = new LocationsView({
					el : '#locations-list',
					collection : this.locations
				});

				// Ad block
				this.AdView = new AdView({
					el : '#sidebar-ad-canvas'
				});
			},

			render : function() {
				// Get template
				this.template = _.template( textSidebarView );
				$(this.el).html(this.template);
				return this;
			},

			onRendered : function() {
				/*
				// Locations list
				var appLocationsView = new LocationsView({
					collection: app.LocationsCollection
				});
				appLocationsView.render();
				$('#locations-list-wrapper').html(appLocationsView.el);
			    // Collapse locations list using metisMenu after adding items to list
			    */
			    console.log('rendered..');
			    $('#locations-list').metisMenu();
			},

			// Locations search
			search : function(event) {
				// Run search
				this.LocationsView.search();
			},

		});

		return SidebarView;
	}
);
