/**
 * APP router
 *
 *
 *
 */ 
define([
		'backbone',
		'app',
		'views/header-view',
		'views/sidebar',
		'views/home-view',
	],
	function(Backbone, app, HeaderView, SidebarView, HomeView) {

		// Router
		var AppRouter = Backbone.Router.extend({

			// Initialise
			initialize : function( options ) {
				// Show work in progress notification when initialising
				var newNotifyView = new app.NotifyView({
					notify : {
						'message' : "This is a work in progress, if you experience a problem please <a href=\"http://about.dumadapt.com#contact\" target=\"_blank\">let us know.</a>"
					}
				});
				// Render
				app.appNotifyView.showView(newNotifyView);
				// Render Headerbar //ALWAYS
				app.appHeaderView.showView(new HeaderView());
				// Render sidebar // ALWAYS
				var sidebarView = new SidebarView();

				Backbone.history.on("route", sendGAPageView);
				// this.bind('all', sendGAPageView);
			},

			// routes
			routes : {
				// Login
				'login' : 'showLoginForm',
				// Signup
				'join' : 'showSignupForm',
				// Recover password
				'recover-password' : 'showPasswordRecoveryForm',
				// Change password
				'change-password' : 'showPasswordChangeForm',
				// Delete Account
				'delete-account' : 'showAccountDeleteForm',
				// Location Details
				'location/:location' : 'showLocationDetails',
				// Bookmark Details
				'bookmark/:bookmark' : 'showBookmarkDetails',
				// default route
				'*action' : 'showHomePage'
			},

			// Default route response
			showHomePage : function( action ) {
				// Render Home view
				app.appMainView.showView(new HomeView());
			},

			// Login form
			showLoginForm : function() {
				require(['views/login'], function(LoginView) {
					// Display login form if user is not logged in
					if (app.SessionModel.get("logged_in") == false) {
						app.appMainView.showView(new LoginView());
					} else {
						/**
						 * @TODO set notifications before redirecting
						 */
						window.location.replace("#/");
					}
				});
			},

			// Signup form
			showSignupForm : function() {
				require(['views/signup-view'], function(SignupView){
					if (app.SessionModel.get("logged_in") == false) {
						app.appMainView.showView(new SignupView());
					} else {
						/**
						 * @TODO set notifications before redirecting
						 */
						window.location.replace("#/");
					}
				});
			},

			showPasswordRecoveryForm : function() {
				require(['views/password-recovery-view'], function(PasswordRecoveryView){
					app.appMainView.showView(new PasswordRecoveryView());
				});
			},

			showPasswordChangeForm : function() {
				require(['views/password-change-view'], function(PasswordChangeView){
					// Only logged in users should change password.
					if (app.SessionModel.get("logged_in")) {
						app.appMainView.showView(new PasswordChangeView());
					} else {
						/**
						 * @TODO set notifications before redirecting
						 */
						window.location.replace("#/");
					}
				});
			},

			showAccountDeleteForm : function() {
				require(['views/delete-account-view'], function(DeleteAccountView){
					// Only logged in users can delete account
					if (app.SessionModel.get("logged_in")) {
						app.appMainView.showView(new DeleteAccountView());
					} else {
						/**
						 * @TODO set notifications before redirecting
						 */
						window.location.replace("#/");
					}
				});
			},

			// Location details
			showLocationDetails : function ( location ) {
				require(['views/location-view'], function(LocationView){
					var locationDetailsView = new LocationView({
						model : app.LocationsCollection.get(location)
					});
					app.appMainView.showView(locationDetailsView);
				});
			},

			// Bookmark details
			showBookmarkDetails : function ( bookmark ) {
				require(['views/bookmark-details-view'], function(BookmarkDetailsView){
					var bookmarkDetailsView = new BookmarkDetailsView({
						model : app.SessionModel.user.bookmarks.get(bookmark)
					});
					app.appMainView.showView(bookmarkDetailsView);
				});
			}

		});

		sendGAPageView = function() {
			url = Backbone.history.getFragment();
			if (typeof ga !== 'undefined') {
				ga('send', 'pageview', "/{url}");
  				// _gaq.push(['_trackPageview', "/#{url}"]);
			}
		}
	
		// Return
		return AppRouter;
	}
);
