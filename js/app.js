/**
 *
 */

define([
		'jquery',
		'underscore',
		'backbone',
		'app-config',
		'metismenu',
		'bootstrap',
		'theme',
		'regula',
	],
	function($, _, Backbone, appConfig, metisMenu) {
		/**
		 *
		 */
		Backbone.View.prototype.close = function() {
			this.remove();
			this.unbind();
			if (this.onClose) {
				this.onClose();
			}
		};

		var app = {

			initialize : function() {
				this.API = appConfig.API;
			},

			/**
			 * Handling display of views
			 */
			//
			appHeaderView : {
				// Show a new view
				showView : function( view ) {
					if (this.currentView) {
						this.currentView.close();
					}
					this.currentView = view;
					this.currentView.render();

					$('#app-header').html(this.currentView.el);

					// onRendered
					if (this.currentView.onRendered) {
						this.currentView.onRendered();
					}
				}
			},
			// div#sidebar
			appSidebarView : {
				// Show a new view
				showView : function( view ) {
					
					if (this.currentView) {
						this.currentView.close();
					}
					this.currentView = view;

					// onRendered
					if (this.currentView.onRendered) {
						this.currentView.onRendered();
					}
				}
			},
			// Notify view div#app-notify
			appNotifyView : {
				// Show a new notification.
				showView : function( view ) {
					
					if (this.currentView) { }
					// Set new notify view as current view
					this.currentView = view;
					// Run view's render function
					this.currentView.render();
					// Append view's el (generated) to notify panel
					$('#app-notify').append(this.currentView.el);

					// Close notify instance after some time
					setTimeout(function(view, self) {
						// Close the view but if it is the current view, then close the current view
						if (self.currentView == view) {
							self.currentView.close();
						} else {
							view.close();
						}
					}, this.currentView.TIMEOUT, view, this);

					// run onRendered callbacks
					if (this.currentView.onRendered) {
						this.currentView.onRendered();
					}
				}
			},
			// div#app
			appMainView : {
				// Show a new view
				showView : function( view ) {
					// Close the current view
					if (this.currentView) {
						this.currentView.close();
					}
					// Replace new view as current view and display
					this.currentView = view;
					this.currentView.render();
					$('#app').html(this.currentView.el);

					// onRendered callback
					if (this.currentView.onRendered) {
						this.currentView.onRendered();
					}
				}
			},

			// Metis Menu
			activateMetisMenu : function( selector, options ) {
				options = options || {};

				selector.removeData("plugin_metisMenu");
				selector.metisMenu( options );
			},

			// fullCalendar configuations
			fullCalendarConfig : {
				header: {
					left: 'prev,today,next',
					center: 'title',
					right: 'agendaDay,agendaWeek,month',
					ignoreTimezone: false
				},
				allDaySlot: false, // Don't show 'all day'
				defaultView : 'agendaWeek',
				// height : 'auto', // Take as much height space as needed
				selectable: false, // No selecting on calendar
				selectHelper: false,
				timeFormat: 'H:mm', // Time 13:00
			},
		}

		// Return
		return app;
	}
);