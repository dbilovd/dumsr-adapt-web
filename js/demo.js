/*
eventsData = "Data";

$.ajax({
			dataType : 'jsonp',
			url : 'http://david-main.local.dev/labs/dumsr/cal/get.php?location=ogbojo',
			success : function() {

			}
			// url : 'js/ecg.json',
		});

*/


$(function() {
	/*
	// Calendar configuration
	fullCalendarConfig = {
		header : {
			left : 'prev, today, next',
			center : "title",
			right : 'month, basicWeek, basicDay',
			// ignoreTimezone : false
		},
		selectable : true,
		selectHelper : true,
		editable : true,
	};

	events = [{"start":"2015-02-25 18:00","end":"2015-02-26 06:00"},{"start":"2015-02-27 06:00","end":"2015-02-27 18:00"},{"start":"2015-02-28 18:00","end":"2015-03-01 06:00"},{"start":"2015-03-02 06:00","end":"2015-03-02 18:00"},{"start":"2015-03-03 18:00","end":"2015-03-04 06:00"},{"start":"2015-03-05 06:00","end":"2015-03-05 18:00"},{"start":"2015-03-06 18:00","end":"2015-03-07 06:00"},{"start":"2015-03-08 06:00","end":"2015-03-08 18:00"},{"start":"2015-03-09 18:00","end":"2015-03-10 06:00"},{"start":"2015-03-11 06:00","end":"2015-03-11 18:00"},{"start":"2015-03-12 18:00","end":"2015-03-13 06:00"},{"start":"2015-03-14 06:00","end":"2015-03-14 18:00"},{"start":"2015-03-15 18:00","end":"2015-03-16 06:00"},{"start":"2015-03-17 06:00","end":"2015-03-17 18:00"},{"start":"2015-03-18 18:00","end":"2015-03-19 06:00"},{"start":"2015-03-20 06:00","end":"2015-03-20 18:00"},{"start":"2015-03-21 18:00","end":"2015-03-22 06:00"},{"start":"2015-03-23 06:00","end":"2015-03-23 18:00"},{"start":"2015-03-24 18:00","end":"2015-03-25 06:00"},{"start":"2015-03-26 06:00","end":"2015-03-26 18:00"}];
	// Get & set JSON data and display calendar
	/*
	$.ajax({
		dataType : 'jsonp',
		url : 'http://david-main.local.dev/laravel_1/public/location/3/schedule',
		success : function( data) {
			for (var i = 0; i < data.schedules.length; i++) {
				events[i] = {
					'title' : 'You have light!',
					'start' : data.schedules[i].start,
					'end' : data.schedules[i].end
				}
			}

			//Add events list to calendar configurations
			fullCalendarConfig.events = events;

			$('#app').fullCalendar(fullCalendarConfig);
		}
		// url : 'js/ecg.json',
	});
	*/

	// $.get("http://david-main.local.dev/laravel_1/public/location/3/schedule", function(data) {
	/*
	$.get("laravel_1/public/location/3/schedule", function(data) {
		for (var i = 0; i < data.schedules.length; i++) {
			events[i] = {
				'title' : 'You have light!',
				'start' : data.schedules[i].start,
				'end' : data.schedules[i].end
			}
		}

		//Add events list to calendar configurations
		fullCalendarConfig.events = events;
		$('#app').fullCalendar(fullCalendarConfig);

	});
	*/

	/*
	for (var i = 0; i < events.length; i++) {
		events[i]['title'] = 'You have light';
	}

	fullCalendarConfig.events = events;
	$('#app').fullCalendar(fullCalendarConfig);
	

	*/

	var Event = Backbone.Model.extend();

	var Events = Backbone.Collection.extend({
		model: Event,
		url : 'laravel_1/public/location/3/schedule'
	});

	var EventsView = Backbone.View.extend({
		initialize: function() {
			_.bindAll(this);

			this.collection.bind('reset', this.addAll);
		},
		render : function() {
			this.$el.fullCalendar({
				header: {
					left: 'prev,today,next',
					center: 'title',
					right: 'month,basicWeek,basicDay',
					ignoreTimezone: false
				},
				selectable: true,
				selectHelper: true,
				editable: true
			});
		},
		addAll : function( ) {
			this.$el.fullCalendar('addEventSource', this.collection.toJSON());
		}
	});

	var events = new Events();
	new EventsView({
		el: '#app',
		collection: events
	}).render();
	events.fetch();
	
});
