define([
        'jquery',
        'bootstrap',
    ],
    function($) {
        // Tooltip
        $('body').tooltip({
            selector : '[data-toggle="tooltip"]'
        });

        //Loads the correct sidebar on window load,
        //collapses the sidebar on window resize.
        // Sets the min-height of #page-wrapper to window size

        bindFunction = function() {
            topOffset = 50;
            width = (this.window.innerWidth > 0) ? this.window.innerWidth : this.screen.width;
            if (width < 768) {
                $('div.navbar-collapse').addClass('collapse')
                topOffset = 100; // 2-row-menu
            } else {
                $('div.navbar-collapse').removeClass('collapse')
            }

            height = (this.window.innerHeight > 0) ? this.window.innerHeight : this.screen.height;
            height = height - topOffset;
            if (height < 1) height = 1;
            if (height > topOffset) {

                // Maximum content of full calendar content
                require(['app'], function(app){
                    app.fullCalendarConfig.contentHeight = height - 140;
                });

                var sidebarHeight = height - (182);
                $("#page-wrapper").css("max-height", (height - 1) + "px");
                $("#sidebar").css("height", sidebarHeight + "px");

                if (width > 768) {
                    // $(".navbar-default.sidebar").css("height", (height - 1) + "px; margin: 0 15px;");
                } else {
                    $("#page-wrapper > div.row").css("margin", "0");
                    $('#countdown-time').css("font-size", "70px");
                    // $(".navbar-default.sidebar").css("height", "100%");
                    // $("#sidebar").css("height", (height - 122) + "px"); // 
                    // $(".sidebar-nav").css("overflow-y", "auto");
                }
            }
        }

        // $(document).ready(bindFunction); // Run on refresh
        $(window).bind("resize", bindFunction); // Bind on any resize that's done after that
    }
)
