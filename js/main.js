
// Configs
require.config({
	// baseUrl : '',
	//	enforceDefine: true,
    waitSeconds: 0, // Wait for each module indefinitely
	// urlArgs: "bust=" + (new Date()).getTime(), // No caching of files (DEV)


	// Paths
	paths : {
		'jquery' : [
			'//code.jquery.com/jquery-1.11.2.min',
			'lib/jquery-1.11.2.min',
		],
		'moment' : [
			'//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment.min',
			'lib/moment.min',
		],
		'fullcalendar' : [
			'//cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.3.1/fullcalendar.min',
			'lib/fullcalendar.min',
		],
		'regula' : 'lib/regula.min',
		'metismenu' : [
			'//cdnjs.cloudflare.com/ajax/libs/metisMenu/2.0.0/metisMenu.min',
			'lib/metisMenu/metisMenu.min',
		],
		'bootstrap' : [
			'//maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min',
			'lib/bootstrap.min',
		],
		'underscore' : [
			'//cdnjs.cloudflare.com/ajax/libs/underscore.js/1.4.4/underscore-min',
			'lib/underscore-min',
		],
		'backbone' : [
			'//cdnjs.cloudflare.com/ajax/libs/backbone.js/0.9.9/backbone-min',
			'lib/backbone-min',
		],
		'backbone.localStorage' : [
			'//cdnjs.cloudflare.com/ajax/libs/backbone-localstorage.js/1.1.14/backbone.localStorage-min',
			'lib/backbone.localStorage.min',
		],
		'app-config' : [
			'appconfig.local',
			'appconfig',
		],
		'text' : 'lib/text',
		'domReady' : 'lib/domReady',
	},
	// Shims
	shim : {
		'underscore' : {
			exports : '_',
		},
		'fullcalendar' : {
			exports: 'fullCalendar'
		},
		'backbone' : {
			deps : ['jquery', 'underscore'],
			exports : 'Backbone',
		},
		'backbone.localStorage' : {
			deps : ['jquery', 'underscore', 'backbone'],
			exports : 'Backbone',
		},
		'bootstrap' : {
			deps : ['jquery'],
		},
		'metismenu' : {
			deps : ['jquery', 'bootstrap'],
			exports : 'metisMenu',
		}
	}
});

// Run app.js
require(['domReady'], function(domReady) {
	domReady(function() {
		// Display App
		require(['app'], function(app) {
			app.initialize();

			// Run App Router
			require(['routes/app'], function(AppRouter) {
				// Instantiate and add app main route to namespace
				app.Router = new AppRouter({
					appMainView : app.appMainView,
					appSidebarView : app.appSidebarView
				});
				// Start backbone history
				if (!Backbone.History.started) {
					Backbone.history.start();
				}
				// Run router (ON NOT AFTER refresh)
				app.Router.navigate(location.hash, true);
			});

		});
		
	});
	
});