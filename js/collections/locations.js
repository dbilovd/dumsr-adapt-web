/**
 * LOCATIONS COLLECTION
 *
 */
define([
		'jquery',
		'underscore',
		'backbone',
		'app',
		'models/location',
		'backbone.localStorage',
	],
	function($, _, Backbone, app, LocationModel) {

		// Escape search string
		RegExp.quote = function(str) {
		    return str.replace(/([.?*+^$[\]\\(){}|-])/g, "\\$1");
		};

		// Locations Collection
		var LocationsCollection = Backbone.Collection.extend({
			url : app.API + '/location', // API url
			model : LocationModel, // Model
			localStorage : new Backbone.LocalStorage('dumAdaptLocations'), // Local Storage

			refreshFromServer : function() {
				return Backbone.ajaxSync('read', this);
			},

			initialize : function() {
				var self = this;
				// Load from localstorage first, if nothing, load from server
				this.fetch();
				if (this.models.length == 0) {
					// Load from Server
					this.refreshFromServer().done(function(result) {
						if (!result.error) {
							self.reset(result.data);
							/**
							 * @todo: find a way to save all server models on reset
							 */
							// Run save on all models
							_.each(self.models, function(model) {
								model.save();
							});
						} else {
							console.log('Error', result);
						}
					});
				}
			},

			// Search
			search : function(searchTerms) {
				// Don't run search on empty string
				if (searchTerms == '') {
					return this;
				}

				// Run search using regular expression and filtering the collection
		 		var pattern = new RegExp(RegExp.quote(searchTerms), 'gi');
				// var pattern = new RegExp(letters, 'gi');
				return _(this.filter(function(data) {
					return pattern.test(data.get("name"));
				}));
			},

			// Region filter
			regionFilter: function(region) {
				return _(this.filter(function(data) {
					// Return locations with ecg_region matching region argument received.
					return data.get("ecg_region") == region;
				}));
			},

			// Search filtered region
			searchFilteredRegion : function(searchTerms, region) {
				// Don't run search on empty string
				if (searchTerms == '') {
					return this.regionFilter(region);
				}

		 		var pattern = new RegExp(RegExp.quote(searchTerms), 'gi');
				return _(this.filter(function(data) {
					return pattern.test(data.get("name")) && data.get("ecg_region") == region;
				}));
			},

		});


		// app.LocationsCollection = new LocationsCollection();

		return LocationsCollection;
	}
);
