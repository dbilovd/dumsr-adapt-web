/**
 *
 */

define([
		'underscore',
		'backbone',
		'app',
		'models/region',
		'backbone.localStorage',
	],
	function(_, Backbone, app, RegionModel) {

		var RegionsCollection = Backbone.Collection.extend({
			// API url
			url : function() {
				return app.API + '/regions';
			},
			
			localStorage : new Backbone.LocalStorage('dumAdaptRegions'),

			refreshFromServer : function() {
				return Backbone.ajaxSync('read', this);
			},

			initialize : function() {
				// Get models
				var self = this;
				// Load from localstorage first, if nothing, load from server
				this.fetch();
				if (this.models.length == 0) {
					// Load from Server
					this.refreshFromServer().done(function(result) {
						if (!result.error) {
							self.reset(result.data);
							/**
							 * @todo: find a way to save all server models on reset
							 */
							// Run save on all models
							_.each(self.models, function(model) {
								model.save();
							});
						} else {
							console.log('Error', result);
						}
					});
					
				}
			},

			// Region filter
			filterByName: function(name) {
				return _(this.filter(function(data) {
					// Return locations with ecg_region matching region argument received.
					return data.get("region") == name;
				}));
			},

		});

		// Regions Collection
		app.RegionsCollection = new RegionsCollection();
		// Return
		return app.RegionsCollection;
	}
);
