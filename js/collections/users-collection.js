/**
 * LOCATIONS COLLECTION
 *
 */

define([
		'underscore',
		'backbone',
		'app',
		'models/user',
	],
	function(_, Backbone, app, UserModel) {

		var UsersCollection = Backbone.Collection.extend({
			// API url
			url : function() {
				return app.API + '/user';
			},
			
			// Model
			model : UserModel,

			// Custom setting of RESTful server response
			
			fetch : function () {
				//var collection = this;
				return Backbone.Collection.prototype.fetch.call(this, {
					success : function(collection, result) {
						if (!result.error && result.data) {
							collection.reset(result.data)
						} else {
							// @todo: Client server error handling.
							console.log(result.messages);
						}
					},
					error : function(collection, result) {

					}
				});
			},
			/*
			parse : function( data ){
				return data.data;
			},
			*/

			initialize : function() {

			},

			// Search
			search : function(letters) {
				// Don't run search on empty string
				if (letters == '') {
					return this;
				}

				// Run search using regular expression and filtering the collection
				var pattern = new RegExp(letters, 'gi');
				return _(this.filter(function(data) {
					return pattern.test(data.get("name"));
				}));
			},

		});

		// Instantiate User Collection
		app.UsersCollection = new UsersCollection();
		app.UsersCollection.fetch();
		return app.UsersCollection;
	}
);
