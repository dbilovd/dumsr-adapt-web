/**
 *
 *
 *
 */

define([
		'backbone',
		'app',
		'models/bookmark',
	],
	function(Backbone, app, BookmarkModel) {

		var BookmarksCollection = Backbone.Collection.extend({
			
			url : (function() {
				// If user is greater than 0
				if (app.SessionModel.get("logged_in") && app.SessionModel.get("user_id") > 0) {
					var url = app.API + '/user/' + app.SessionModel.get("user_id") + '/bookmark';
					return url;
				}

				return '';
			}),
			

			// Custom setting of RESTful server response
			
			fetch : function() {
				return Backbone.Collection.prototype.fetch.call(this, {
					success : function(collection, result) {
						if (!result.error && result.data) {
							collection.reset(result.data)
						} else {
							// @todo: Client server error handling.
							console.log(result.messages);
						}
					},
					error : function(collection, result) {

					}
				});
			},
			
			// Custom setting of RESTful server response
			/*
			parse : function( data ) {
				console.log(data);
				return data.data;
			},
			*/

			// Localstorage
			// localStorage : new Backbone.LocalStorage("dumsradaptSavedLocations"),

			// Default Model
			model : BookmarkModel,

			initialize : function() {
				// Event bindings
				_.bindAll(this);
				// app.SessionModel.bind("change", this.fetchOverride);
				// app.SessionModel.bind("change", this.fetchOverride);
				// Fetch models
				// this.fetch();

			},

			// Custom fetch
			fetchOverride : function() {
				// Fetch if user is logged in.
				if (app.SessionModel.get("logged_in")) {
					this.fetch();
				} else {
					// Destroy current collections object.
					this.reset();
				}
			},

			updateModels : function() {
				console.log("Updating models", this.models);
			}

		});

		return BookmarksCollection;
	}
);
