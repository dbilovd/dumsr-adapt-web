
define([
		'jquery',
		'underscore',
		'backbone',
		'app',
	],
	function($, _, Backbone, app) {

		var SchedulesCollection = Backbone.Collection.extend({
			model : ScheduleModel,
			
			// location : 0,

			url : function() {
				return app.API + '/location/' + this.location + '/schedule';
			},

			// Custom fetch
			
			fetch : function() {
				return Backbone.Collection.prototype.fetch.call(this, {
					success : function(collection, result) {
						if (!result.error && result.data) {
							collection.reset(result.data);
						} else {
							// @todo: Client server error handling.
							console.log(result.messages);
						}
					},
					error : function(collection, result) {
						console.log("error");
					}
				});
			},
			/*
			parse : function( data ){
				return data.data;
			}
			*/

			initialize : function() {
			},

		});

		// Instantiate and add to namespace
		// app.SchedulesCollection = new SchedulesCollection({});
		return SchedulesCollection;
	}
)
