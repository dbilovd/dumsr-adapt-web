
define([
		'jquery',
		'underscore',
		'backbone',
		'app'
	],
	function($, _, Backbone, app){

		var ScheduleModel = Backbone.Model.extend({
			url : '',
			
			// Custom fetch
			
			fetch : function() {

				return Backbone.Model.prototype.fetch.call(this, {
					success : function(model, result) {
						console.log("model", model);
						console.log("result", result);
						if (!result.error && result.data) {
							model.set(result.data);
						} else {
							// @todo: Client server error handling.
							console.log(result.messages);
						}
					},
					error : function(model, result) {

					}
				});
			},
			/*
			parse : function( data ){
				return data.data;
			},
			*/

			defaults : {
				title : '',
				start : '0000-00-00 00:00',
				end : '0000-00-00 00:00'
			},

			initialize : function() {
				this.title = this.get("title");
				this.start = this.get("start");
				this.end = this.get("end");
			}

		});

		return ScheduleModel;
	}
);
