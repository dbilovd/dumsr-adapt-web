/**
 * LOCATION MODEL
 *
 *
 */

define([
		'jquery',
		'underscore',
		'backbone',
		'app',
	],
	function($, _, Backbone, app){

		var LocationModel = Backbone.Model.extend({
			url : '',
			// Schedules URL
			schedulesUrl : function() {
				return app.API + '/location/' + this.id + '/schedule';
			},
			
			// Custom fetch
			fetch : function() {

				return Backbone.Model.prototype.fetch.call(this, {
					success : function(model, result) {
						if (!result.error && result.data) {
							model.set(result.data);
						} else {
							// @todo: Client server error handling.
							console.log(result.messages);
						}
					},
					error : function(model, result) {

					}
				});
			},
			/*
			parse : function( data ){
				return data.data;
			},
			*/

			// Default values
			defaults : {
				name : '',
				lat : 0.00,
				lon : 0.00,
				ecg_region : '',
				ecg_group : '',
				first_light_on : '0000-00-00 00:00:00',
				first_light_off : '0000-00-00 00:00:00',
				second_light_on : '0000-00-00 00:00:00',
				created_at : '0000-00-00 00:00:00',
				updated_at : '0000-00-00 00:00:00',
				schedules : {}
			},

			// Initialize function
			initialize : function() {
			},

			// Get schedule from server
			fetchSchedules : function( options, callbacks ) {
				// var fetchUrl = app.API + '/location/' + this.id + '/schedule';
				var fetchUrl = this.schedulesUrl();
				var options = options || {};

				// Request
		        $.ajax({
		        	// Construct url for request based on method
		            // url : app.API + '/location/' + this.id + '/schedule',
		            url : fetchUrl,
		            type : 'GET',
		            contentType: 'application/json', // JSON server
		            dataType: 'json',
		            data:  options, // submit data as a JSON string

		            // CSRF
		            /*
		            beforeSend: function(xhr) {
		                // Set the CSRF Token in the header for security
		                var token = $('meta[name="csrf-token"]').attr('content');
		                if (token) xhr.setRequestHeader('X-CSRF-Token', token);
		            },
		            */

		            // Successfull posted and received result from server
		            success : function(result){
		                // Run callbacks if any
		                if (callbacks && 'success' in callbacks) {
		                  	callbacks.success(result);
		                }
		            },
		            // Failed to contact server
		            error: function(result){
		                console.log(model, result);
		                // Run callbacks if any
		                if (callbacks && 'error' in callbacks) {
		                  	callbacks.error(result);
		                }
		            }
		        })
				// requesting completed
				.complete(function() {});
			},

			// Get next schedule
			fetchNextSchedule : function( options, callbacks ) {
				var options = options || {};
				var self = this;
				var next = options.next ? options.next : true;
				// Current Date
				var currentDate = new Date(),
					currentDateDay = currentDate.getDate(),
					currentDateMonth = currentDate.getMonth() + 1,
					currentDateYear = currentDate.getFullYear(),
					currentDateHour = currentDate.getHours(),
					currentDateMinute = currentDate.getMinutes();

				this.fetchSchedules(
					{
						'times' : 1,
						'start' : currentDateYear + '-' + currentDateMonth + '-' + currentDateDay + ' ' + currentDateHour + ':' + currentDateMinute
					},
					// Callbacks
					callbacks
				);
			}

		});

		return LocationModel;
	}
);
