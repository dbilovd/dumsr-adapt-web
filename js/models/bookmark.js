/**
 * BOOKMARK MODEL
 *
 *
 */
define([
		'jquery',
		'underscore',
		'backbone',
		'app'
	],
	function($, _, Backbone, app) {
		var BookmarkModel = Backbone.Model.extend({
			// Bookmark URL
			/*
			url : function() {
				// If user is greater than 0
				if (app.SessionModel.get("logged_in") && app.SessionModel.get("user_id") > 0) {
					// var url = app.API + '/user/' + app.SessionModel.get("user_id") + '/bookmark/' + this.get("id");
					var url = app.API + '/user/' + app.SessionModel.get("user_id") + '/bookmark/' + (this.id || '');
					console.log(url);
					return url;
				}
				return '';
			},
			*/
			// Schedules URL
			schedulesUrl : function() {
				return app.API + '/location/' + this.get("location") + '/schedule';
			},

			// Custom fetch
			
			fetch : function() {
				return Backbone.Model.prototype.fetch.call(this, {
					success : function(model, result) {
						if (!result.error && result.data) {
							model.set(result.data);
						} else {
							// @todo: Client server error handling.
							console.log(result.messages);
						}
					},
					error : function(model, result) {

					}
				});
			},
			/*
			parse : function( data ){
				return data.data;
			},
			*/

			defaults : {
				// id : '',
				name : "",
				user : 0,
				location : 0,
				// created_at : "0000-00-00 00:00:00",
				// updated_at : "0000-00-00 00:00:00",
			},

			// Validation
			validate : function() {},

			// Initialize function
			initialize : function() {
			
			},

			// Get schedule from server
			fetchSchedules : function( options, callbacks ) {

				// Request
		        $.ajax({
		        	// Construct url for request based on method
		            url : this.schedulesUrl(),
		            type : 'GET',
		            contentType: 'application/json', // JSON server
		            dataType: 'json',

		            // CSRF
		            /*
		            beforeSend: function(xhr) {
		                // Set the CSRF Token in the header for security
		                var token = $('meta[name="csrf-token"]').attr('content');
		                if (token) xhr.setRequestHeader('X-CSRF-Token', token);
		            },
		            */

		            // Successfull posted and received result from server
		            success : function(result){
		                // Run callbacks if any
		                if (callbacks && 'success' in callbacks) {
		                  	callbacks.success(result);
		                }
		            },
		            // Failed to contact server
		            error: function(result){
		                console.log(model, result);
		                // Run callbacks if any
		                if (callbacks && 'error' in callbacks) {
		                  	callbacks.error(result);
		                }
		            }
		        })
				// requesting completed
				.complete(function() {});
			}

		});

		return BookmarkModel;
	}
);
