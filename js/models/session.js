/**
 *
 */

define([
        'jquery',
        'underscore',
        'backbone',
        'app',
        'models/user',
        'collections/users-collection',
    ],
    function($, _, Backbone, app, UserModel, UsersCollection) {

        var SessionModel = Backbone.Model.extend({
            // URL
            url : function() {
                return app.API + '/user';
            },

            // Defaults
            defaults : {
                logged_in : false,
                user_id : 0
            },

            // Initialize
            initialize : function() {
                // Set user for this session
                this.user = new UserModel({});

                // If session storage
                if (window.localStorage) {
                    // Session details
                    if (localStorage.dumsrAdaptSession) {
                        var dumsrAdaptSession = localStorage.getItem("dumsrAdaptSession");
                        dumsrAdaptSession = JSON.parse(dumsrAdaptSession);
                        // Update session model
                        this.set(dumsrAdaptSession);
                    }

                    // Session user
                    if (app.UsersCollection.get(this.get("user_id"))) {
                        this.user = app.UsersCollection.get(this.get("user_id"));
                    } else {
                        // This user model wouldn't be added to the user's collection
                        // or saved, because it's only being used for this session.
                        this.user = new UserModel();
                    }

                    // Session User's details
                    if (localStorage.dumsrAdaptSessionUser) {
                        // Get user details from localStorage if available
                        var dumsrAdaptSessionUser = localStorage.getItem("dumsrAdaptSessionUser");
                        // Parse details
                        dumsrAdaptSessionUser = JSON.parse(dumsrAdaptSessionUser);
                        // Update user's session
                        this.updateSessionUser(dumsrAdaptSessionUser);
                    } else {
                        // Authenticate again on server to get user's details
                        this.checkAuth();
                    }
                }

                // Update localStorage on change
                _.bindAll(this);
                this.bind('change', this.updateSessionStorage);

                // After logged in user is established load bookmarks
                if (this.get("logged_in")) {
                    this.user.bookmarks.url = app.API + '/user/' + this.user.id + '/bookmark';
                    this.user.bookmarks.fetch();
                }
                this.bind('change', this.user.bookmarks.fetchOverride);
                this.user.bookmarks.bind('add', function(bookmark) {
                    // New Bookmark notification   
                });
            },

            // Update sessions user attributes with server's response
            updateSessionUser : function( userData ) {
                if ( userData ) {
                    this.user.set(_.pick(userData, _.keys(this.user.defaults)));

                    // Store user details locally
                    try {
                        localStorage.setItem("dumsrAdaptSessionUser", JSON.stringify(this.user.attributes));
                    } catch(error) {
                        console.log(error);
                    }
                } else {
                    console.log("Error occured while starting up a session for you. (Client Error)");
                }
            },

            // Update local storage session
            updateSessionStorage : function( ) {
                if (window.localStorage) {
                    var sessionToStore = {
                        'logged_in' : this.get("logged_in"),
                        'user_id' : this.get('user_id'),
                    };
                    // Store session
                    try {
                        localStorage.setItem("dumsrAdaptSession", JSON.stringify(sessionToStore));
                    } catch(error) {
                        console.log(error);
                    }
                }
            },

            /**
             * Check
             */
            checkAuth : function(callbacks, args) {
                // Reference variable to this object
                var self = this;

                this.fetch({
                    url : this.url() + '/login', // User login url (GET)

                    // Fetched successfully
                    success : function(model, result) {
                        // Server authentication successful
                        if (!result.error && result.data.logged_in) {
                            // Update sessions user
                            self.updateSessionUser(result.data.user);
                            self.set({
                                logged_in : true,  // Log in session
                                user_id : result.data.user.id, // User ID
                            });
                            // Run callbacks
                            if (callbacks && 'success' in callbacks) {
                                callbacks.success(model, result);
                            }
                            
                        } else { // Server authentication failed.
                            var newUser = new UserModel({});
                            self.updateSessionUser(newUser.attributes);
                            self.set({
                                logged_in : false, // Log out session
                                // Remove auth_token
                            });
                            // Run error callbacks
                            if (callbacks && 'error' in callbacks) {
                                callbacks.error(model, result);
                            }
                        }
                    },
                    // Error while fetching
                    error : function(model, result) {
                        // Log out session
                        var newUser = new UserModel({});
                        self.updateSessionUser(newUser.attributes);
                        self.set({
                            user_id : 0,
                            logged_in : false
                        });
                        // Run error callbacks
                        if (callbacks && 'error' in callbacks) {
                            callbacks.error(model, result);
                        }
                    }
                })
                // Fetch completed.
                .complete(function(){
                    // update session saved locally
                    self.updateSessionStorage();

                    // Run complete callbacks if any
                    if (callbacks && 'complete' in callbacks) {
                        callbacks.complete(model, result);
                    }
                });
            },

            /**
             * POST
             */
            postAuth : function(options, callbacks, args) {
                // Reference variable to this object
                var self = this;
                // Run authentication on server again
                this.checkAuth();
                // POST data
                var postData = _.omit(options, 'method'); // Remove 'method' from options
                // if(DEBUG) console.log(postData);
                // var postType = (args.postType) ? args.postType : 'POST';
                // console.log(postType);
                
                // Run request
                $.ajax({
                    // Construct url for request based on method
                    url : this.url() + '/' + options.method,
                    type : 'POST',
                    contentType: 'application/json', // JSON server
                    dataType: 'json',
                    data:  JSON.stringify( _.omit(options, 'method') ), // POST data as a JSON string

                    // CSRF
                    /*
                    beforeSend: function(xhr) {
                        // Set the CSRF Token in the header for security
                        var token = $('meta[name="csrf-token"]').attr('content');
                        if (token) xhr.setRequestHeader('X-CSRF-Token', token);
                    },
                    */

                    // Successfull posted and received result from server
                    success : function(result){
                        // server auth successful
                        if ( result.error == false ) {
                            // On logout and remove_account
                            if (_.indexOf(['remove-account', 'logout'], options.method) !== -1) {
                                var newUser = new UserModel({});
                                self.updateSessionUser(newUser.attributes);
                                self.set({
                                    user_id : 0,
                                    logged_in: false
                                });
                            } else { // On login and signup
                                // Update session's user
                                self.updateSessionUser( result.data.user );
                                // Login session
                                self.set({
                                    user_id: result.data.user.id,
                                    logged_in: true
                                });
                            }
                            // Run callbacks
                            if (callbacks && 'success' in callbacks) {
                                callbacks.success(result);
                            }
                        } else { // Server authentcation failed.
                            // console.log(result.messages);
                            // Run callbacks
                            if(callbacks && 'error' in callbacks) {
                                callbacks.error(result);
                            }
                        }
                    },
                    // Failed to contact server
                    error: function(model, result){
                        // Run callbacks
                        if(callbacks && 'error' in callbacks) {
                            callbacks.error(result);
                        }
                    }
                })
                // POSTing completed
                .complete(function() {
                    // Update localStorage variables
                    self.updateSessionStorage();
                    // Run callbacks if any
                    if(callbacks && 'complete' in callbacks) {
                        callbacks.complete(result);
                    }
                });

            },

            /**
             * Session Interface Methods
             */
            // Login
            login : function(options, callbacks, args){
                this.postAuth(_.extend(options, {
                    method: 'login' 
                }), callbacks);
            },

            // Login
            loginValidate : function(options, callbacks, args){
                this.postAuth(_.extend(options, {
                    method: 'login-validate' 
                }), callbacks);
            },

            // Logout
            logout : function(options, callbacks, args){
                var newUser = new UserModel({});
                this.updateSessionUser(newUser.attributes);
                this.set({
                    user_id : 0,
                    logged_in : false, // Log out session
                });

                // Run callbacks
                /*
                if (callbacks && 'success' in callbacks) {
                    callbacks.success();
                }
                if (callbacks && 'error' in callbacks) {
                    callbacks.error();
                }
                */
                
                // Logout on server
                
                this.postAuth(_.extend(options, { 
                    method: 'logout' 
                }), callbacks);

            },

            // Signup
            signup : function(options, callbacks, args){
                this.postAuth(_.extend(options, {
                    method: 'create' 
                }), callbacks);
            },

            // Recover password
            recoverPassword : function(options, callbacks, args) {
                this.postAuth(_.extend(options, { 
                    method: 'recover-password' 
                }), callbacks);
            },
            // Change password
            changePassword : function(options, callbacks, args) {
                this.postAuth(_.extend(options, {
                    method : 'change-password'
                }), callbacks);
            },

            // Delete account
            removeAccount : function(options, callbacks, args){
                this.postAuth(_.extend(options, { 
                    method: 'remove-account' 
                }), callbacks);
            }

        });

        // Session Model
        app.SessionModel = new SessionModel();
    }
);
