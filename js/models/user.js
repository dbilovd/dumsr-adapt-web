/**
 *
 */

define([
		'backbone',
		'app',
		'collections/bookmarks'
	],
	function(Backbone, app, BookmarksCollection) {

		var UserModel = Backbone.Model.extend({
			// URL
			url : function() {
				return app.API + '/user/' + this.id;
			},
			
			// Custom fetch
			fetch : function() {

				return Backbone.Model.prototype.fetch.call(this, {
					success : function(model, result) {
						if (!result.error && result.data) {
							model.set(result.data);
						} else {
							// @todo: Client server error handling.
							console.log(result.messages);
						}
					},
					error : function(model, result) {

					}
				});
			},
			
			/*
			parse : function( data ){
				return data.data;
			},
			*/

			// Default
			defaults : {
				id : 0,
				email : "",
		        username : "",
		        first_name : "",
		        last_name : "",
			},

			// Initialize
			initialize : function() {
				// Lazy load handling of user's saved location
				this.bookmarks = new BookmarksCollection();
			},

			/** HELPER FUNCTIONS **/
			getFullName : function() {
				var fullName = "",
					first_name = this.get('first_name'),
					last_name = this.get('last_name'),
					username = this.get('username');

				if (first_name != "" || last_name != "") {
					fullName = ((first_name) ? first_name : "") + " " + ((last_name) ? last_name : "");
				} else {
					fullName = username;
				}
				return fullName;
			}
		});

		return UserModel;
	}
);
