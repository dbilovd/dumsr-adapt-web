/**
 * Region MODEL
 *
 *
 */

define([
		'jquery',
		'underscore',
		'backbone',
		'app',
	],
	function($, _, Backbone, app) {

		var RegionModel = Backbone.Model.extend({
			url : '',

			// Custom fetch
			
			fetch : function() {

				return Backbone.Model.prototype.fetch.call(this, {
					success : function(model, result) {
						if (!result.error && result.data) {
							model.set(result.data);
						} else {
							// @todo: Client server error handling.
							console.log(result.messages);
						}
					},
					error : function(model, result) {

					}
				});
			},
			
			/*
			parse : function( data ){
				return data.data;
			},
			*/

			// Default values
			defaults : {
				region : '',
				group : '',
				first_light_on : '0000-00-00 00:00:00',
				first_light_off : '0000-00-00 00:00:00',
				second_light_on : '0000-00-00 00:00:00',
				created_at : '0000-00-00 00:00:00',
				updated_at : '0000-00-00 00:00:00'
			},

			// Initialize function
			initialize : function() {

			},

		});

		return RegionModel;
	}
);
