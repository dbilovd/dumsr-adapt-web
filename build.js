({	
	
	mainConfigFile : 'js/main.js',
	baseUrl : 'js',
	name : 'main',
	//out : 'dist/main.js',
	dir : 'dist',
	removeCombined : true,
	findNestedDependencies : true,
	paths : {
		'jquery' : 'empty:',
		'moment' : 'empty:',
		'fullcalendar' : 'empty:',
		// 'regula' : 'empty:',
		'metismenu' : 'empty:',
		'bootstrap' : 'empty:',
		'underscore' : 'empty:',
		'backbone' : 'empty:',
		'backbone.localStorage' : 'empty:',
		'app-config' : 'empty:',
		// 'text' : 'empty:',
		// 'domReady' : 'empty:',
	}
});
